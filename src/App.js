import { useState, useEffect } from "react";

import { BrowserRouter, Routes, Route } from "react-router-dom";

import { Box, createTheme, ThemeProvider } from "@mui/material";
import Navbar from "./components/Navbar";
import Home from "./pages/Home";
import Products from "./pages/Products";
import NotFound from "./pages/NotFound";
import Property from "./pages/Property";
import Login from "./pages/Login";

function App() {
  const [mode, setMode] = useState("light");
  const [isUserLogged, setIsUserLogged] = useState(false);

  useEffect(() => {
    const state = localStorage.getItem("isLogged");
    if (state === "yes") {
      setIsUserLogged(true);
    }
  }, []);

  const theme = createTheme({
    palette: {
      mode: mode,
    },
  });

  const handleLogin = (state) => {
    setIsUserLogged(state);
    localStorage.setItem("isLogged", "yes");
  };
  return (
    <ThemeProvider theme={theme}>
      <Box bgcolor={"background.default"} color={"text.primary"}>
        {isUserLogged ? (
          <BrowserRouter>
            <Routes>
              <Route index element={<Home />} />
              <Route path="/property/:id" element={<Property />} />
              <Route path="*" element={<NotFound />} />
            </Routes>
          </BrowserRouter>
        ) : (
          <Login handleLogin={handleLogin} />
        )}
      </Box>
    </ThemeProvider>
  );
}

export default App;

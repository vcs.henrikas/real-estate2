import { Container, Grid, Typography } from "@mui/material";
import React from "react";
import rectangle from "../assets/images/Rectangle3.jpg";
import check from "../assets/images/check.svg";

export default function ChooseUs() {
  return (
    <Container maxWidth="xl">
      <Typography
        sx={{ fontSize: "38px", textAlign: "center", margin: "70px auto" }}
      >
        Why you should choose us.
      </Typography>
      <Grid container>
        <Grid xs={12} lg={6} sx={{ display: "flex", justifyContent: "center" }}>
          <img src={rectangle} alt="image" />
        </Grid>
        <Grid xs={12} lg={6}>
          <Typography
            sx={{
              fontSize: "21px",
              lineHeight: "28px",
              maxWidth: "489px",
              "@media (max-width: 1200px)": { marginTop: "50px" },
              margin: "0 auto",
            }}
            variant="subtitle1"
          >
            Creating quality urban lifestyles, building stronger communities and
            creating a safe haven for the general populace is what we do, we
            give affordabel house for sell, rent and also lease great
            propertities for any kind of usage.
          </Typography>
          <Typography
            sx={{
              fontSize: "21px",
              lineHeight: "28px",
              maxWidth: "489px",
              margin: "0 auto",
              marginTop: "30px",
            }}
            variant="subtitle1"
          >
            we give premium offers to all our client and our customer service is
            top notch
          </Typography>
          <Grid
            container
            maxWidth={489}
            sx={{ margin: "0 auto", marginTop: "50px" }}
          >
            <Grid item xs={6} sx={{ display: "flex" }}>
              <img src={check} alt="check" />
              <Typography
                variant="subtitle1"
                sx={{ fontWeight: "500", fontSize: "21px", marginLeft: "15px" }}
              >
                World class
              </Typography>
            </Grid>
            <Grid item xs={6} sx={{ display: "flex" }}>
              <img src={check} alt="check" />
              <Typography
                variant="subtitle1"
                sx={{ fontWeight: "500", fontSize: "21px", marginLeft: "15px" }}
              >
                World class
              </Typography>
            </Grid>
            <Grid item xs={6} sx={{ display: "flex" }}>
              <img src={check} alt="check" />
              <Typography
                variant="subtitle1"
                sx={{ fontWeight: "500", fontSize: "21px", marginLeft: "15px" }}
              >
                World class
              </Typography>
            </Grid>
            <Grid item xs={6} sx={{ display: "flex" }}>
              <img src={check} alt="check" />
              <Typography
                variant="subtitle1"
                sx={{ fontWeight: "500", fontSize: "21px", marginLeft: "15px" }}
              >
                World class
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
}

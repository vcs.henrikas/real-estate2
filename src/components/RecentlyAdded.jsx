import { Box, Container, Typography } from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";

export default function RecentlyAdded() {
  return (
    <Container maxWidth="xl" sx={{ marginTop: "50px" }}>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Typography sx={{ fontSize: "38px", fontWeight: "700" }}>
          Recently Added
        </Typography>
        <Link to="/" style={{ textDecoration: "none" }}>
          <Typography
            sx={{
              fontSize: "24px",
              color: "#E90808",
              fontWeight: "700",
              textDecoration: "none",
            }}
          >
            Show all
          </Typography>
        </Link>
      </Box>
    </Container>
  );
}

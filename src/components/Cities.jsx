import {
  Box,
  Button,
  Card,
  CardContent,
  CardMedia,
  Container,
  Grid,
  Typography,
} from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { propertiesActions } from "../store/store";

export default function Cities() {
  const cities = useSelector((state) => state);

  return (
    <Container maxWidth="xl" sx={{ marginTop: "70px" }}>
      <Typography
        variant="h3"
        sx={{
          fontSize: "38px",
          maxWidth: "521px",
          textAlign: "center",
          margin: "0 auto",
        }}
      >
        We are available in different cities across the globe.
      </Typography>
      <Grid container spacing={2} sx={{ marginTop: "70px" }}>
        {cities.map((city, index) => {
          return index < 4 ? (
            <Grid item xs={12} md={6} key={city.id}>
              <Card maxWidth={587}>
                <CardMedia
                  component="img"
                  alt="first city"
                  height="386"
                  image={city.image}
                />
                <CardContent
                  sx={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <Box sx={{ display: "flex" }}>
                    <Typography
                      sx={{ fontSize: "38px", marginRight: ".5rem" }}
                      variant="subtitle1"
                    >
                      {city.city},
                    </Typography>
                    <Typography
                      sx={{ fontSize: "38px", color: "#8F90A6" }}
                      variant="subtitle1"
                    >
                      {" "}
                      {city.country}
                    </Typography>
                  </Box>
                  <Link
                    to={`/property/${city.id}`}
                    style={{ textDecoration: "none" }}
                  >
                    <Button variant="contained" color="error">
                      Explore
                    </Button>
                  </Link>
                </CardContent>
              </Card>
            </Grid>
          ) : null;
        })}
      </Grid>
    </Container>
  );
}

import { Container, Grid, Typography } from "@mui/material";
import React from "react";

export default function Statistics() {
  return (
    <Container sx={{ marginTop: "54px" }} maxWidth="xl">
      <Grid container>
        <Grid
          item
          xs={12}
          md={6}
          lg={3}
          sx={{
            padding: "12px 50px 12px 50px",
            "@media (max-width: 1200px)": {
              borderLeft: 3,
              borderColor: "red",
            },
          }}
        >
          <Typography variant="body1" sx={{ fontSize: "30px" }}>
            1000+
          </Typography>
          <Typography
            variant="body1"
            sx={{
              color: "#8F90A6",
              marginTop: "13px",
              fontSize: "30px",
            }}
          >
            Premium Houses
          </Typography>
        </Grid>
        <Grid
          item
          xs={12}
          md={6}
          lg={3}
          sx={{
            borderLeft: 3,
            borderColor: "red",
            padding: "12px 50px 12px 50px",
          }}
        >
          <Typography variant="body1" sx={{ fontSize: "30px" }}>
            1000+
          </Typography>
          <Typography
            variant="body1"
            sx={{
              color: "#8F90A6",
              marginTop: "13px",
              fontSize: "30px",
            }}
          >
            Premium Houses
          </Typography>
        </Grid>
        <Grid
          item
          xs={12}
          md={6}
          lg={3}
          sx={{
            borderLeft: 3,
            borderColor: "red",
            padding: "12px 50px 12px 50px",
          }}
        >
          <Typography variant="body1" sx={{ fontSize: "30px" }}>
            1000+
          </Typography>
          <Typography
            variant="body1"
            sx={{
              color: "#8F90A6",
              marginTop: "13px",
              fontSize: "30px",
            }}
          >
            Premium Houses
          </Typography>
        </Grid>
        <Grid
          item
          xs={12}
          md={6}
          lg={3}
          sx={{
            borderLeft: 3,
            borderColor: "red",
            padding: "12px 50px 12px 50px",
          }}
        >
          <Typography variant="body1" sx={{ fontSize: "30px" }}>
            1000+
          </Typography>
          <Typography
            variant="body1"
            sx={{
              color: "#8F90A6",
              marginTop: "13px",
              fontSize: "30px",
            }}
          >
            Premium Houses
          </Typography>
        </Grid>
      </Grid>
    </Container>
  );
}

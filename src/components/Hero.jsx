import React from "react";
import { Box, Container, Typography } from "@mui/material";
import heroImage from "../assets/images/hero.png";
import Filters from "./Filters";

export default function Hero() {
  return (
    <Container
      maxWidth="xl"
      sx={{
        marginTop: "55px",
        paddingTop: "110px",
        paddingBottom: "110px",
        backgroundImage: `linear-gradient(to right, white, transparent), url(${heroImage})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPosition: "center",
        borderRadius: "15px",
      }}
    >
      <Box sx={{ maxWidth: "600px" }}>
        <Typography variant="h1" sx={{ fontSize: "56px" }}>
          Simple way to find the perfect property
        </Typography>
        <Typography
          variant="body1"
          sx={{ fontSize: "22px", marginTop: "25px", marginBottom: "37px" }}
        >
          We provide a complete service for the sale, purchase or rental of real
          estate.
        </Typography>
        <Filters />
      </Box>
    </Container>
  );
}

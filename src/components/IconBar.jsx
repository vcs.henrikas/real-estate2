import { Container, Box, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import FacebookIcon from "@mui/icons-material/Facebook";
import InstgramIcon from "@mui/icons-material/Instagram";
import TwitterIcon from "@mui/icons-material/Twitter";
import LanguageIcon from "@mui/icons-material/Language";
import Phone from "@mui/icons-material/Phone";

export default function IconBar() {
  const iconsStyle = {
    color: "#E90808",
    marginRight: "53px",
  };

  // for different screen sizes
  const [width, setWidth] = useState(window.innerWidth);

  useEffect(() => {
    const handleResize = () => {
      setWidth(window.innerWidth);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <Container
      sx={{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
      }}
    >
      {width > 480 ? (
        <>
          <Box sx={{ padding: "50px 0" }}>
            <FacebookIcon sx={iconsStyle} />
            <InstgramIcon sx={iconsStyle} />
            <TwitterIcon sx={iconsStyle} />
            <LanguageIcon sx={iconsStyle} />
          </Box>
          <Box sx={{ display: "flex", padding: "50px 0" }}>
            <Phone sx={{ color: "#E90808" }} />
            <Typography>+37061111111</Typography>
          </Box>
        </>
      ) : (
        ""
      )}
    </Container>
  );
}

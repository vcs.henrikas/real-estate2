import { Box, Button, Container, Typography } from "@mui/material";
import React, { useState, useEffect } from "react";
import IconBar from "./IconBar";
import logo from "../assets/images/logo.svg";
import MenuIcon from "@mui/icons-material/Menu";

export default function Navbar() {
  const [width, setWidth] = useState(window.innerWidth);

  useEffect(() => {
    const handleResize = () => {
      setWidth(window.innerWidth);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  return (
    <>
      <IconBar />
      <Container
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Box sx={{ display: "flex", height: "100%", alignItems: "center" }}>
          <img src={logo} />
          <Typography
            sx={{ marginLeft: "5", fontWeight: 700, fontSize: "20px" }}
          >
            Real Estate Gig
          </Typography>
        </Box>
        {width > 960 ? (
          <Box sx={{ display: "flex", height: "100%", alignItems: "center" }}>
            {["HOME", "AGENTS", "PROPERTIES", "BLOG"].map((item) => (
              <Typography
                sx={{ marginRight: "60px", letterSpacing: "0.2rem" }}
                key={item}
              >
                {item}
              </Typography>
            ))}
            <Button variant="contained" color="error">
              Contact
            </Button>
          </Box>
        ) : (
          <MenuIcon />
        )}
      </Container>
    </>
  );
}

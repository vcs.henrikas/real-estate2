import {
  Box,
  Button,
  Container,
  Grid,
  MenuItem,
  Select,
  TextField,
  Typography,
  Slider,
} from "@mui/material";
import React, { useState } from "react";
import dollar from "../assets/images/dollar-sign.svg";
import { useSelector, useDispatch } from "react-redux";
import { propertiesActions } from "../store/store";

function valuetext(value) {
  return `${value}°C`;
}
export default function Filters() {
  const [addType, setAddType] = useState("Rent");

  const [value, setValue] = React.useState([20, 37]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const cities = useSelector((state) => state);
  return (
    <Container
      sx={{
        width: "984px",
        backgroundColor: "#FFF",
        padding: "25px",
        paddingRight: "36px",
        borderRadius: "17px",
        boxShadow: "0px 21px 43px rgba(233, 8, 8, 0.1)",
      }}
    >
      <Grid container>
        <Grid
          container
          xs={10}
          rowSpacing={2}
          // sx={{ display: "flex", justifyContent: "space-between" }}
        >
          <Grid item xs={12} md={3}>
            {/* pirmas grid elementas */}
            <Select
              sx={{
                borderRadius: "22px",
                width: "120px",
                height: "42px",
                backgroundColor: "#F4F4F4",
                border: "none",
              }}
              value={addType}
              onChange={(e) => {
                setAddType(e.target.value);
              }}
              renderValue={(value) => {
                return (
                  <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
                    <img src={dollar} alt="house with dollar icon" />
                    {value}
                  </Box>
                );
              }}
            >
              <MenuItem value={"Rent"}>Rent</MenuItem>
              <MenuItem value={"Buy"}>Buy</MenuItem>
            </Select>
          </Grid>
          <Grid item xs={12} md={6}>
            {/* antras grid elementas */}
            <TextField
              label="Enter location"
              sx={{ width: "100%" }}
              variant="filled"
              InputProps={{
                disableUnderline: true,
                style: {
                  width: "100%",
                  height: "42px",
                  borderRadius: "22px",
                  backgroundColor: "#F4F4F4",
                  border: "None",
                },
              }}
            ></TextField>
          </Grid>
          <Grid
            item
            xs={12}
            md={3}
            sx={{ display: "flex", justifyContent: "end" }}
          >
            {/* trecias grid elementas */}
            <Select
              sx={{
                borderRadius: "22px",
                width: "120px",
                height: "42px",
                backgroundColor: "#F4F4F4",
                border: "none",
              }}
              value={addType}
              onChange={(e) => {
                setAddType(e.target.value);
              }}
              renderValue={(value) => {
                return (
                  <Box
                    sx={{
                      display: "flex",
                      gap: 1,
                      alignItems: "center",
                      border: "none!important",
                    }}
                  >
                    <img src={dollar} alt="house with dollar icon" />
                    {value}
                  </Box>
                );
              }}
            >
              <MenuItem value={"Rent"}>Rent</MenuItem>
              <MenuItem value={"Buy"}>Buy</MenuItem>
            </Select>
          </Grid>
          {/* apatinis filtru aukstas */}
          <Grid xs={3} item>
            <Typography>Price range</Typography>
          </Grid>
          <Grid item xs={6}>
            <Slider
              getAriaLabel={() => "Temperature range"}
              value={value}
              onChange={handleChange}
              valueLabelDisplay="auto"
              getAriaValueText={valuetext}
              color="error"
            />
          </Grid>
          <Grid item xs={3} sx={{ display: "flex", justifyContent: "end" }}>
            <Select
              sx={{
                borderRadius: "22px",
                width: "120px",
                height: "42px",
                backgroundColor: "#F4F4F4",
                border: "none",
              }}
              value={addType}
              onChange={(e) => {
                setAddType(e.target.value);
              }}
              renderValue={(value) => {
                return (
                  <Box
                    sx={{
                      display: "flex",
                      gap: 1,
                      alignItems: "center",
                      border: "none!important",
                    }}
                  >
                    <img src={dollar} alt="house with dollar icon" />
                    {value}
                  </Box>
                );
              }}
            >
              <MenuItem value={"Rent"}>Rent</MenuItem>
              <MenuItem value={"Buy"}>Buy</MenuItem>
            </Select>
          </Grid>
          <Grid item xs={2}>
            <Select
              sx={{
                borderRadius: "22px",
                width: "120px",
                height: "32px",
                backgroundColor: "#F4F4F4",
                border: "none",
              }}
              value={addType}
              onChange={(e) => {
                setAddType(e.target.value);
              }}
              renderValue={(value) => {
                return (
                  <Box
                    sx={{
                      display: "flex",
                      gap: 1,
                      alignItems: "center",
                      border: "none!important",
                    }}
                  >
                    <img src={dollar} alt="house with dollar icon" />
                    {value}
                  </Box>
                );
              }}
            >
              <MenuItem value={"Rent"}>Rent</MenuItem>
              <MenuItem value={"Buy"}>Buy</MenuItem>
            </Select>
          </Grid>
          {/* apatinis filtru aukstas */}
        </Grid>
        <Grid
          item
          xs={2}
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Typography variant="h3" sx={{ fontSize: "20px", fontWeight: 700 }}>
            {cities.length}
          </Typography>
          <Typography variant="body1" color="989898">
            Results
          </Typography>
          <Button color="error" variant="contained" sx={{ marginTop: "23px" }}>
            Search
          </Button>
        </Grid>
      </Grid>
    </Container>
  );
}

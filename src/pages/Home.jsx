import React from "react";
import ChooseUs from "../components/ChooseUs";
import Cities from "../components/Cities";
import Hero from "../components/Hero";
import Navbar from "../components/Navbar";
import RecentlyAdded from "../components/RecentlyAdded";
import Statistics from "../components/Statistics";

export default function Home() {
  return (
    <>
      <Navbar />
      <Hero />
      <Statistics />
      <Cities />
      <ChooseUs />
      <RecentlyAdded />
    </>
  );
}

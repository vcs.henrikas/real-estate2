import {
  Box,
  Button,
  Container,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import React, { useRef, useState } from "react";

export default function Login(props) {
  const usernameRef = useRef(null);
  const passwordRef = useRef(null);
  const [signUp, setSignUp] = useState(false);

  const handleLogin = () => {
    if (
      usernameRef.current.value === localStorage.getItem("username") &&
      passwordRef.current.value === localStorage.getItem("password")
    ) {
      props.handleLogin(true);
    }
  };

  const handleSignUp = () => {
    if (signUp) {
      if (
        usernameRef.current.value.length > 6 &&
        passwordRef.current.value.length > 6
      ) {
        localStorage.setItem("username", usernameRef.current.value);
        localStorage.setItem("password", passwordRef.current.value);
        setSignUp(false);
        usernameRef.current.value = "";
        passwordRef.current.value = "";
      }
    } else {
      setSignUp(true);
    }
  };
  return (
    <Container sx={{ display: "grid", placeItems: "center", height: "100vh" }}>
      <Box
        sx={{
          backgroundColor: "#F6F6F6",
          padding: "30px",
          borderRadius: "20px",
        }}
      >
        <Grid container sx={{ display: "flex", justifyContent: "center" }}>
          <Grid item xs={12}>
            <Typography
              variant="h3"
              sx={{ textAlign: "center", marginBottom: "22px" }}
            >
              {signUp ? "Sign Up" : "Login"}
            </Typography>
          </Grid>
          <Grid
            item
            xs={12}
            sx={{
              display: "flex",
              justifyContent: "center",
              marginBottom: "13px",
            }}
          >
            <TextField
              sx={{ borderRadius: "20px" }}
              label="Enter username"
              inputRef={usernameRef}
              InputProps={{
                disableUnderline: true,
                style: {
                  height: "42px",
                  borderRadius: "20px",
                  backgroundColor: "#F6F6F6",
                  borderColor: "#828282",
                },
              }}
            ></TextField>
          </Grid>
          <Grid item xs={12} sx={{ display: "flex", justifyContent: "center" }}>
            <TextField
              sx={{ borderRadius: "20px" }}
              label="Password"
              inputRef={passwordRef}
              InputProps={{
                disableUnderline: true,
                style: {
                  height: "42px",
                  borderRadius: "20px",
                  backgroundColor: "#F6F6F6",
                  borderColor: "#828282",
                },
              }}
            ></TextField>
          </Grid>
          <Grid item xs={12} sx={{ marginTop: "13px" }}>
            <Typography
              variant="body1"
              color="red"
              sx={{ textAlign: "center" }}
            >
              Forgot password?
            </Typography>
          </Grid>
          <Grid
            item
            xs={4}
            sx={{ display: "flex", justifyContent: "space-between" }}
          >
            <Button
              variant="contained"
              sx={{ borderRadius: "20px" }}
              onClick={handleLogin}
            >
              Login
            </Button>
            <Button
              variant="contained"
              sx={{ borderRadius: "20px" }}
              onClick={handleSignUp}
            >
              Sign Up
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
}

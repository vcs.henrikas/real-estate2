import React from "react";
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { propertiesActions } from "../store/store";
import { Typography, Container } from "@mui/material";

export default function Property() {
  const cities = useSelector((state) => state);
  const { id } = useParams();
  const property = cities.find((city) => city.id === Number(id));
  return (
    <Container>
      <Typography>{property.city}</Typography>
      <Typography>{property.country}</Typography>
    </Container>
  );
}

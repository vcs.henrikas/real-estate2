import { createSlice, configureStore } from "@reduxjs/toolkit";

const cities = [
  {
    id: 1,
    city: "Vilnius",
    country: "Lithuania",
    image:
      "https://images.pexels.com/photos/3113867/pexels-photo-3113867.png?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
    description: "aprasymas",
    price: 280000,
    address: "Vilnius",
    rooms: 3,
    area: 50,
    floor: "1/5",
  },
  {
    id: 2,
    city: "Kaunas",
    country: "Lithuania",
    image:
      "https://images.pexels.com/photos/4013242/pexels-photo-4013242.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 3,
    city: "Klaipėda",
    country: "Lithuania",
    image:
      "https://images.pexels.com/photos/6141314/pexels-photo-6141314.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 4,
    city: "Nida",
    country: "Lithuania",
    image:
      "https://images.pexels.com/photos/2903268/pexels-photo-2903268.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 5,
    city: "Riga",
    country: "Latvia",
    image:
      "https://images.pexels.com/photos/2102659/pexels-photo-2102659.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
  },
  {
    id: 6,
    city: "Tallinn",
    country: "Estonia",
    image:
      "https://images.pexels.com/photos/2841096/pexels-photo-2841096.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 7,
    city: "Helsinki",
    country: "Finland",
    image:
      "https://images.pexels.com/photos/2102690/pexels-photo-2102690.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
  },
  {
    id: 8,
    city: "Stockholm",
    country: "Sweden",
    image:
      "https://images.pexels.com/photos/3737548/pexels-photo-3737548.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=1260",
  },
  {
    id: 9,
    city: "Oslo",
    country: "Norway",
    image:
      "https://images.pexels.com/photos/1092751/pexels-photo-1092751.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 10,
    city: "Copenhagen",
    country: "Denmark",
    image:
      "https://images.pexels.com/photos/620320/pexels-photo-620320.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=1260",
  },
  {
    id: 11,
    city: "Barcelona",
    country: "Spain",
    image:
      "https://images.pexels.com/photos/793213/pexels-photo-793213.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 12,
    city: "Madrid",
    country: "Spain",
    image:
      "https://images.pexels.com/photos/2962820/pexels-photo-2962820.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 13,
    city: "Seville",
    country: "Spain",
    image:
      "https://images.pexels.com/photos/1628070/pexels-photo-1628070.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 14,
    city: "Valencia",
    country: "Spain",
    image:
      "https://images.pexels.com/photos/2853522/pexels-photo-2853522.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 15,
    city: "Lisbon",
    country: "Portugal",
    image:
      "https://images.pexels.com/photos/2852948/pexels-photo-2852948.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 16,
    city: "Porto",
    country: "Portugal",
    image:
      "https://images.pexels.com/photos/4772750/pexels-photo-4772750.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 17,
    city: "Paris",
    country: "France",
    image:
      "https://images.pexels.com/photos/106877/pexels-photo-106877.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 18,
    city: "Nice",
    country: "France",
    image:
      "https://images.pexels.com/photos/1592932/pexels-photo-1592932.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 19,
    city: "Cannes",
    country: "France",
    image:
      "https://images.pexels.com/photos/1416383/pexels-photo-1416383.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 20,
    city: "Amsterdam",
    country: "Netherlands",
    image:
      "https://images.pexels.com/photos/2398301/pexels-photo-2398301.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 21,
    city: "Rotterdam",
    country: "Netherlands",
    image:
      "https://images.pexels.com/photos/1487781/pexels-photo-1487781.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 22,
    city: "The Hague",
    country: "Netherlands",
    image:
      "https://images.pexels.com/photos/101064/pexels-photo-101064.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 23,
    city: "Brussels",
    country: "Belgium",
    image:
      "https://images.pexels.com/photos/4173233/pexels-photo-4173233.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 28,
    city: "Hamburg",
    country: "Germany",
    image:
      "https://images.pexels.com/photos/4283398/pexels-photo-4283398.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 29,
    city: "Copenhagen",
    country: "Denmark",
    image:
      "https://images.pexels.com/photos/1837423/pexels-photo-1837423.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 30,
    city: "Oslo",
    country: "Norway",
    image:
      "https://images.pexels.com/photos/2772858/pexels-photo-2772858.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 31,
    city: "Stockholm",
    country: "Sweden",
    image:
      "https://images.pexels.com/photos/2098449/pexels-photo-2098449.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 32,
    city: "Helsinki",
    country: "Finland",
    image:
      "https://images.pexels.com/photos/240040/pexels-photo-240040.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 33,
    city: "Reykjavik",
    country: "Iceland",
    image:
      "https://images.pexels.com/photos/33043/reykjavik-iceland-houses-traditional.jpg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 34,
    city: "Dublin",
    country: "Ireland",
    image:
      "https://images.pexels.com/photos/1555815/pexels-photo-1555815.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 35,
    city: "Edinburgh",
    country: "Scotland",
    image:
      "https://images.pexels.com/photos/3990409/pexels-photo-3990409.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 36,
    city: "Barcelona",
    country: "Spain",
    image:
      "https://images.pexels.com/photos/2707117/pexels-photo-2707117.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 37,
    city: "Madrid",
    country: "Spain",
    image:
      "https://images.pexels.com/photos/247700/pexels-photo-247700.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
];

const propertiesSlice = createSlice({
  name: "properties",
  initialState: cities,
});

const store = configureStore({
  reducer: propertiesSlice.reducer,
});

export const propertiesActions = propertiesSlice.actions;
export default store;
